
if(process.env.NODE_ENV === 'production')
	module.exports = require('./prod-secrets.js')
else
	module.exports = require('./dev-secrets.js')
