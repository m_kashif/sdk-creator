
const axios = require('axios')
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const path = require('path')

const app = express()
const port = process.env.PORT || 5000

const TITAN_TOKEN = require('./secrets').TITAN_TOKEN
const TITAN_URL = require('./secrets').TITAN_URL

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.post('/titan/verify-token', (req, res) => {
	if(req.body.token !== TITAN_TOKEN)
		res.json({mismatch: true})
	else
		res.json({success: true})
})

app.post('/titan/trigger', (req, res) => {

	const {flavour, sdkType, commit, token, sourceSets} = req.body

	if(token !== TITAN_TOKEN)
		return res.json({mismatch: true})

	const webhookData = {
		repository: {
			name: 'hyper-sdk-android',
			links: {
				html: {
					href: 'https://bitbucket.org/juspay/hyper-sdk-android'
				}
			}
		},
		sourceSets,
		sdkType,
		commit,
		flavour
	}

	console.log('\n\nWEBHOOK DATA\n')
	console.log(webhookData)

	// trigger titan process
	axios.post(
		`${TITAN_URL}?token=${token}`,
		JSON.stringify(webhookData),
		{headers: {'Content-Type': 'application/json'}}
	)
		.then(webhookRes => {
			console.log(webhookRes.data)
			res.json({success: true})
		})
		.catch(err => {
			console.log(err)
			res.json({success: false})
		})
})

app.get('/', (req, res) => {
	app.use(express.static('build'))
	res.sendFile(path.resolve(__dirname, 'build', 'index.html'))  
})

app.listen(port, () => console.log(`Listening on port ${port}.`))
