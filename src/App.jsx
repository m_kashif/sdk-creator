
import React, { Component } from 'react'
import { hot } from 'react-hot-loader/root'

const defaultCode = "\
releaseDexGuard.assets.srcDir 'src/release/assets'\n\
\n\
checkout.assets.srcDirs 'src/upiintent/assets', 'src/ec/assets', 'src/vies/assets', 'src/dotp/assets'\n\
checkout.res.srcDirs 'src/upiintent/res', 'src/vies/res', 'src/dotp/res'\n\
\n\
ec.assets.srcDirs  'src/upiintent/assets', 'src/gpay/assets', 'src/dotp/assets'\n\
ec.res.srcDirs 'src/upiintent/res', 'src/dotp/res'\n\
\n\
hypersdk.assets.srcDirs 'src/jiosaavn/assets'\n\
hypersdk.res.srcDir 'src/jiosaavn/res'"

const defaultPlaceholder = `Paste sourceSets code here\n  file: hyper-sdk/build.gradle\n\nDefault code: \n\n${defaultCode}`

class App extends Component {

    state = {
        editorRows: 15,
        editorColumns: 0,
        SDKStatus: 'Create SDK',
        err: false,
        showTokenInput: false,
        data: {
            sourceSets: '',
            sdkType: 'development',
            commit: '',
            flavour: 'hypersdk'
        }
    }

    updateEditor = () => {
        let w = window.innerWidth

        if(w <= 500)
            this.setState({editorColumns: w / 11})
        else
            this.setState({editorColumns: w / 13})
    }

    updateFrames = (key) => {
        if(key.keyCode === 27 && this.state.showTokenInput)
            this.setState({
                err: false,
                showTokenInput: false
            })
    }

    componentDidMount() {
        window.addEventListener('keydown', this.updateFrames)
        window.addEventListener('resize', this.updateEditor)
        this.updateEditor()
    }

    componentWillUnmount() {
        window.removeEventListener('resize')
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.data.sourceSets !== this.state.data.sourceSets) {

            const codeLen = this.state.data.sourceSets.split(/\r\n|\r|\n/).length
            let editorRows = 0
            if(codeLen === 1 && this.state.data.sourceSets.length === 0)
                editorRows = 15
            else if(codeLen >= 15)
                editorRows = 15
            else
                editorRows = codeLen

            this.setState({editorRows: editorRows})

        }

        if(prevState.SDKStatus !== 'Create SDK')
            if(JSON.stringify(prevState.data) !== JSON.stringify(this.state.data))
                this.setState({SDKStatus: 'Create SDK'})
    }

    verifyUser = () => {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({token: this.token.value})
        }
        fetch('http://localhost:5000/titan/verify-token', requestOptions)
            .then(response => response.json())
            .then(data => {
                if(data.success)
                    this.sendTriggerRequest()
                else if(data.mismatch)
                    this.setState({err: 'Please check the password'})
                else if(data.invalid)
                    this.setState({err: 'Maximum trials exceeded. Try again in some time'})
            })
    }

    sendTriggerRequest = () => {

        const data = {...this.state.data}
        data.token = this.token.value

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }
        fetch('http://localhost:5000/titan/trigger', requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setState({showTokenInput: false})
                if(data)
                if(data.success)
                    this.setState({ SDKStatus: 'An email will be sent on successful SDK generation.' })
                else if(data.mismatch)
                    this.setState({ SDKStatus: 'Invalid password. Please try again.' })
                else
                    this.setState({ SDKStatus: 'Jenkins process could not be triggered. Do it manually.' })
            })
            .catch(err => {
                this.setState({
                    SDKStatus: 'Jenkins process could not be triggered. Do it manually.',
                    showTokenInput: false
                })
                console.log(err.response.data)
            })
    }

    onChange = event => {

        if(event.target.id === 'commit' || event.target.id === 'sourceSets') {

            if(event.target.id === 'commit')
                if(event.target.value.length === 0)
                    document.getElementById('commit').style.width = '23ch'
                else document.getElementById('commit').style.width = `${event.target.value.length + 5}ch`

            const data = {...this.state.data}
            data[event.target.id] = event.target.value
            this.setState({data})
        } else {

            this.setState({
                [event.target.id]: event.target.value,
                SDKStatus: 'Create SDK'
            })
        }
    }

    render() {

        const {err, editorColumns, editorRows, SDKStatus, showTokenInput, data} = this.state
        const {commit, flavour, sdkType, sourceSets} = data
        let createSDKClass = 'create'
        if(sourceSets.length === 0)
            createSDKClass += ' disabled'
        else if(SDKStatus === 'An email will be sent on successful SDK generation.')
            createSDKClass += ' success'
        else if(SDKStatus === 'Jenkins process could not be triggered. Do it manually.' || SDKStatus === 'Invalid password. Please try again.')
            createSDKClass += ' error'

        return (
            <div className="app">
                <div className="heading">
                    Juspay Payment SDKs
                </div>
                <div className="source-set">
                    <div className="build_options">
                        <div className="heading">Build Options</div>
                        <div className="sdk-type">
                            <label>SDK type</label>
                            <div className="options">
                                <span className={sdkType === 'development' ? 'selected' : ''}
                                    onClick={() => {
                                        const data = {...this.state.data}
                                        data.sdkType = 'development'
                                        this.setState({data})
                                    }} >Development</span>
                                <span className={sdkType === 'production' ? 'selected' : ''}
                                    onClick={() => {
                                        const data = {...this.state.data}
                                        data.sdkType = 'production'
                                        this.setState({data})
                                    }} >Production</span>
                            </div>
                        </div>
                        <div className="flavour">
                            <label>Flavour</label>
                            <div className="options">
                                <span className={flavour === 'hypersdk' ? 'selected' : ''}
                                    onClick={() => {
                                        const data = {...this.state.data}
                                        data.flavour = 'hypersdk'
                                        this.setState({data})
                                    }} >hypersdk</span>
                                <span className={flavour === 'internalEc' ? 'selected' : ''}
                                    onClick={() => {
                                        const data = {...this.state.data}
                                        data.flavour = 'internalEc'
                                        this.setState({data})
                                    }} >internalEc</span>
                            </div>
                        </div>
                        <div className="commit">
                            <label>Commit message</label>
                            <div className="options">
                                <input type="text" placeholder='Commit messsage' id='commit'
                                    onFocus={() => {
                                        const commitNode = document.getElementById('commit')
                                        if(commitNode.style.backgroundColor === 'rgb(204, 63, 12)') {
                                            commitNode.value = ''
                                            commitNode.style.backgroundColor = '#EFEFEF'
                                            commitNode.style.color = '#000000'
                                        }
                                    }}
                                    onChange={e => this.onChange(e)} />
                            </div>
                        </div>
                    </div>
                    <div className="editor">
                        <div>
                            sourceSets &#123;
                            <div className={`load${this.state.data.sourceSets === defaultCode ? ' disabled' : ''}`}
                                onClick={() => {
                                    document.getElementById('sourceSets').value = defaultCode
                                    const data = {...this.state.data}
                                    data.sourceSets = defaultCode
                                    this.setState({data})
                                }} >Load default code</div>
                        </div>
                        <textarea id='sourceSets'
                            rows={editorRows} cols={editorColumns}
                            placeholder={defaultPlaceholder}
                            onChange={event => this.onChange(event)} ></textarea>
                        <div>&#125;</div>
                    </div>
                    <button className={createSDKClass} disabled={this.state.data.sourceSets.length === 0 || SDKStatus !== 'Create SDK'}
                        onClick={() => {
                            const commitNode = document.getElementById('commit')
                            if(commitNode.value.length !== 0 && commitNode.style.backgroundColor !== 'rgb(204, 63, 12)')
                                this.setState({showTokenInput: true})
                            else {
                                commitNode.style.backgroundColor = '#CC3F0C'
                                commitNode.style.color = '#FFFFFF'
                                commitNode.value = 'Enter commit message'
                            }
                        }} >
                        {
                            SDKStatus === 'An email will be sent on successful SDK generation.' ||
                            SDKStatus === 'Jenkins process could not be triggered. Do it manually.'
                            ? <img src={require('./assets/images/information.svg')} alt="i" />
                            : null
                        }
                        {SDKStatus}
                    </button>
                </div>

                {
                    showTokenInput ?
                    <div className="token-input">
                        <img src={require('./assets/images/close.svg')}
                            onClick={() => this.setState({showTokenInput: false})} />
                        <input type="password" ref={node => this.token = node}
                            placeholder='Password?' autoFocus
                            onKeyDown={(e) => {
                                if(e.keyCode === 13)
                                    this.verifyUser()
                                if(this.state.err)
                                    this.setState({err: false})
                            }} />
                        { err ? <div className="err" onClick={() => this.setState({err: false})} >
                                    {err}
                                    <img src={require('./assets/images/cross.svg')} alt="" />
                                </div> : null }
                        <div className="submit" onClick={this.verifyUser} >VERIFY</div>
                    </div>
                    : null
                }
            </div>
        )
    }
}

export default hot(App)
